function tmux_sockets
    for socket in (lsof -U 2> /dev/null | grep tmux-1 | sed 's|^.*tmux-1[0-9]\+/\([0-9]*\) .*$|\1|' | uniq)
        echo $socket
    end
end
